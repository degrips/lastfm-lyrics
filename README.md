# Last.fm lyrics
Python script which displays lyrics (from Genius.com) to the currently scrobbling song on Last.fm in your terminal.  

This can be used with any music player that has scrobbling support.

To get lyrics from genius, you need to install johnwmillr's [LyricsGenius](https://github.com/johnwmillr/LyricsGenius) python wrapper.  

I used code from 0nse's [Now Playing](https://github.com/0nse/now_playing) script to get song information from Last.fm.  

# Usage
1. Get api keys from [Last.fm](https://www.last.fm/api/account/create) and [Genius](https://genius.com/api-clients/new).  
It doesn't matter what you write to get the keys.  

2. Insert api keys and username into the script.  

3. Install johnwmillr's [LyricsGenius](https://github.com/johnwmillr/LyricsGenius) python wrapper

4. Execute script '$ python PATH/TO/SCRIPT'  
